package com.shiva.virtusacodetest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shiva.virtusacodetest.R;
import com.shiva.virtusacodetest.data.model.Item;

import java.text.DateFormat;
import java.util.List;

/**
 * Created by Shiva on 1/28/18.
 */

public class IsslistAdapter extends RecyclerView.Adapter<IsslistAdapter.ViewHolder> {

    private List<Item> mItems;
    private Context mContext;
    private PostItemListener mItemListener;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView titleTv;
        public TextView resetTime;

        PostItemListener mItemListener;

        public ViewHolder(View itemView, PostItemListener postItemListener) {
            super(itemView);
            titleTv = (TextView) itemView.findViewById(R.id.tv_duriation);
            resetTime = (TextView) itemView.findViewById(R.id.tv_reset_time);

            this.mItemListener = postItemListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Item item = getItem(getAdapterPosition());
            this.mItemListener.onPostClick(Integer.toString(item.getRisetime()));
        }
    }

    public IsslistAdapter(Context context, List<Item> posts, PostItemListener itemListener) {
        mItems = posts;
        mContext = context;
        mItemListener = itemListener;
    }

    @Override
    public IsslistAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View postView = inflater.inflate(R.layout.list_row_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(postView, this.mItemListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(IsslistAdapter.ViewHolder holder, int position) {

        Item item = mItems.get(position);
        TextView duriation = holder.titleTv;
        TextView resetTime = holder.resetTime;
        duriation.setText("" + item.getDuration());
        resetTime.setText(DateFormat.getTimeInstance().format(item.getRisetime()));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void updateAnswers(List<Item> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    private Item getItem(int adapterPosition) {
        return mItems.get(adapterPosition);
    }

    public interface PostItemListener {
        void onPostClick(String id);
    }

}
