package com.shiva.virtusacodetest.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shiva on 1/28/18.
 */

public class Item {

    @SerializedName("duration")
    @Expose
    private int duration;

    @SerializedName("risetime")
    @Expose
    private int risetime;

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getRisetime() {
        return risetime;
    }

    public void setRisetime(int risetime) {
        this.risetime = risetime;
    }

}
