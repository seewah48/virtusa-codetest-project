package com.shiva.virtusacodetest.data.remote;

import com.shiva.virtusacodetest.data.model.ISSResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Shiva on 1/28/18.
 */

public interface ISSService {

    @GET("/iss-pass.json")
    Call<ISSResponse> getAnswers(@QueryMap Map<String, Double> options);

}