package com.shiva.virtusacodetest.data.model;

import android.content.ClipData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Shiva on 1/28/18.
 */

public class ISSResponse {

    @SerializedName("response")
    @Expose
    private List<Item> response = null;

    /**
     * @return The response
     */

    public List<Item> getResponse() {
        return response;
    }

    /**
     * @param response The response
     */
    public void setResponse(List<Item> response) {
        this.response = response;
    }
}