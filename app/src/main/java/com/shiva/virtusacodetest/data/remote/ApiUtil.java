package com.shiva.virtusacodetest.data.remote;

/**
 * Created by Manasa on 1/28/18.
 */

public class ApiUtil {

    public static final String BASE_URL = "http://api.open-notify.org/";

    public static ISSService getISSService() {

        return RetrofitClient.getClient(BASE_URL).create(ISSService.class);
    }
}
